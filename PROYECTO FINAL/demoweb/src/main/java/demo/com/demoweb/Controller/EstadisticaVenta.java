package demo.com.demoweb.Controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

@RestController
public class EstadisticaVenta {
    @RequestMapping("/estadistica")
    public ArrayList<ReporteVentas> Statistics() throws  Exception{
        ArrayList<ReporteVentas> inventarioVentas=new ArrayList<>();
        int idsede=0,idarea=0,cantidad,max=0,codprod,codprodmax=0;
        String nombsede=null,nombarea=null,nombprod=null;
        float costo,costomax=0;
        Class.forName("org.postgresql.Driver");
        Connection conexion = DriverManager.getConnection("jdbc:postgresql://3.94.184.53:5432/vzehanvo",
                "vzehanvo", "zz2KifILhIh-PF6kTH-MbDfFtkS3O5OT");
        Statement st = conexion.createStatement();
        String sql="SELECT *FROM VENDIDO";
        ResultSet rs=st.executeQuery(sql);
        while(rs.next()){
            cantidad=rs.getInt(3);
            codprod=rs.getInt(2);
            costo=rs.getFloat(4);
            if(cantidad>=max){
                max=cantidad;
                codprodmax=codprod;
                costomax=costo;
            }
        }
        rs.close();
        st.close();
        Statement st2=conexion.createStatement();
        String sql1="SELECT*FROM PRODUCTO WHERE codprod=";
        sql1+=codprodmax;
        ResultSet rs2=st2.executeQuery(sql1);
        while(rs2.next()){
            idsede=rs2.getInt(1);
            idarea=rs2.getInt(2);
            nombprod=rs2.getString(4);
        }
        rs2.close();
        st2.close();
        Statement st3=conexion.createStatement();
        String sql2="SELECT*FROM AREA WHERE codarea=";
        sql2+=idarea;
        ResultSet rs3=st3.executeQuery(sql2);
        while(rs3.next()){
            nombarea=rs3.getString(3);
        }
        Statement st4=conexion.createStatement();
        String sql3="SELECT*FROM SEDE WHERE idsede=";
        sql3+=idsede;
        ResultSet rs4=st4.executeQuery(sql3);
        while(rs4.next()){
            nombsede=rs4.getString(1);
        }
        int i=0;
        ReporteVentas InformeDeVentas;
        InformeDeVentas =Informe(nombprod, nombarea, nombsede, max, costomax);
        inventarioVentas.add(i,InformeDeVentas);
        return inventarioVentas;
    }
    public ReporteVentas Informe(String nompr,String nomar,String nomsede,int cant, float cost){
        ReporteVentas Informeaux=new ReporteVentas();
        Informeaux.setCosto(cost);
        Informeaux.setCantidad(cant);
        Informeaux.setNombrearea(nomar);
        Informeaux.setNombreproducto(nompr);
        Informeaux.setNombresede(nomsede);
        return Informeaux;
    }
}
