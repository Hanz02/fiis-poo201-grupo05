package demo.com.demoweb.Controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.sql.*;
import java.util.ArrayList;

@RestController
public class Inventario {
    @RequestMapping("/inventario")
    public ArrayList<ObtenerInventario> inventarioE() throws Exception {
        int aux1,aux4,aux7,idsede1,idsede2,idsede3,idarea1,idarea2,idarea3;
        float aux2,aux5,aux8;
        String aux3,aux6,aux9;
        ArrayList<ObtenerInventario> arregloinventario = new ArrayList<>();
        Class.forName("org.postgresql.Driver");
        Connection c = DriverManager.getConnection("jdbc:postgresql://3.94.184.53:5432/vzehanvo",
                "vzehanvo", "zz2KifILhIh-PF6kTH-MbDfFtkS3O5OT");
        Statement st = c.createStatement();
        String sql1 = "SELECT * FROM producto WHERE idSede=100";
        ResultSet rs = st.executeQuery(sql1);
        int i=0;
        //OBTENGO LOS DATOS DE LA TABLA PRODUCTO
            while (rs.next()) {
                idsede1=rs.getInt(1);
                idarea1=rs.getInt(2);
                aux3 = rs.getString(4);
                aux1 = rs.getInt(5);
                aux2 = rs.getFloat(6);
                ObtenerInventario resultado;
                    resultado = AlmacenoProducto(idsede1,idarea1, aux3, aux1, aux2);
                arregloinventario.add(i, resultado);
                i++;
            }

        //PRIMERO CIERRA EL RESULSET PARA NO GENERAR UNA EXCEPCION
        rs.close();
        st.close();
        Statement st2=c.createStatement();
        String sql2="select*from producto where idsede=200";
        ResultSet rs2=st2.executeQuery(sql2);
        while(rs2.next()){
            idsede2=rs2.getInt(1);
            idarea2=rs2.getInt(2);
            aux6=rs2.getString(4);
            aux4=rs2.getInt(5);
            aux5=rs2.getFloat(6);
            ObtenerInventario resultado2;
            resultado2=AlmacenoProducto(idsede2,idarea2,aux6, aux4, aux5);
            arregloinventario.add(resultado2);
        }
        rs2.close();
        st2.close();
        Statement st3=c.createStatement();
        String sql3="select*from producto where idsede=300";
        ResultSet rs3=st3.executeQuery(sql3);
        while(rs3.next()){
            idsede3=rs3.getInt(1);
            idarea3=rs3.getInt(2);
            aux9=rs3.getString(4);
            aux7=rs3.getInt(5);
            aux8=rs3.getFloat(6);
            ObtenerInventario resultado3;
            resultado3=AlmacenoProducto(idsede3,idarea3,aux9, aux7, aux8);
            arregloinventario.add(resultado3);
        }
        rs3.close();
        st3.close();
        c.close();
        return arregloinventario;
    }
    public ObtenerInventario AlmacenoProducto(int idsede,int idarea,String nombreP,int existencia,float costo){
        ObtenerInventario prueba = new ObtenerInventario();
        prueba.setIdsede(idsede);
        prueba.setIdarea(idarea);
        prueba.setNombreprod(nombreP);
        prueba.setExistencias(existencia);
        prueba.setCosto(costo);
        return prueba;
    }
}
