package demo.com.demoweb.Controller;

public class ObtenerVenta {
    String nombComp,apelComp,dir,nombprod;
    int idventa,idsede;
    float costo;

    public String getNombComp() {
        return nombComp;
    }

    public void setNombComp(String nombComp) {
        this.nombComp = nombComp;
    }

    public String getApelComp() {
        return apelComp;
    }

    public void setApelComp(String apelComp) {
        this.apelComp = apelComp;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getNombprod() {
        return nombprod;
    }

    public void setNombprod(String nombprod) {
        this.nombprod = nombprod;
    }

    public int getIdventa() {
        return idventa;
    }

    public void setIdventa(int idventa) {
        this.idventa = idventa;
    }

    public int getIdsede() {
        return idsede;
    }

    public void setIdsede(int idsede) {
        this.idsede = idsede;
    }

    public float getCosto() {
        return costo;
    }

    public void setCosto(float costo) {
        this.costo = costo;
    }
}
