package demo.com.demoweb.Controller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.*;
import java.util.ArrayList;

@Controller
//Ingresa los datos del cliente y redirige a la parte venta para que escoja el producto que desea.
public class Vendedor {
    @RequestMapping("/insertarVendedor")

    public String Vendedor(@RequestParam int idVend, @RequestParam String Nom, @RequestParam String Apell, @RequestParam String Direc,
                           @RequestParam int idSede) throws Exception {
        int aux2;
        boolean validar1;
        ArrayList<Boolean> validar = new ArrayList<>();
        Class.forName("org.postgresql.Driver");
        Connection cone = DriverManager.getConnection("jdbc:postgresql://3.94.184.53:5432/vzehanvo",
                "vzehanvo", "zz2KifILhIh-PF6kTH-MbDfFtkS3O5OT");
        Statement st = cone.createStatement();
        String sql = "SELECT idsede FROM sede";
        ResultSet rs = st.executeQuery(sql);
        //valido la existencia de la sede ingresada por el administrador
        while (rs.next()) {
            aux2 = rs.getInt(1);
            validar1 = Valida(idSede, aux2);
            validar.add(validar1);
        }
        rs.close();
        st.close();
        if (validar.contains(true)) {
            //Como el idsede ingresado existe entonces recien se puede añadir datos a la tabla vendedor
            String sql3 = "INSERT INTO VENDEDOR VALUES (?,?,?,?,?)";
            PreparedStatement pst = cone.prepareStatement(sql3);
            pst.setInt(1, idVend);
            pst.setString(2, Nom);
            pst.setString(3, Apell);
            pst.setString(4, Direc);
            pst.setInt(5, idSede);
            pst.executeUpdate();
            cone.close();
            return "redirect:/venta.html";
        } else {
            return "ID SEDE NO EXISTE";
        }
    }

    public Boolean Valida(int codigoIngresa, int codigoBD) {
        //se puede acortar el codigo a una sola linea pero lo deje asi para diferenciar vendedor de venta
        if (codigoBD == codigoIngresa) {
            return true;
        } else {
            return false;
        }
    }
}