package demo.com.demoweb.Controller;



import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;



import java.sql.*;
import java.util.ArrayList;
@Controller
public class Venta {
    @RequestMapping("/Venta")
    public String insertarVenta(@RequestParam int idvent, @RequestParam int idProd, @RequestParam int cantidad) throws Exception {
        int existencias = 0, id, stock;
        float costo = 0;
        boolean validarid;
        ArrayList<Boolean> validacion = new ArrayList<>();
        Class.forName("org.postgresql.Driver");
        Connection conexion = DriverManager.getConnection("jdbc:postgresql://3.94.184.53:5432/vzehanvo",
                "vzehanvo", "zz2KifILhIh-PF6kTH-MbDfFtkS3O5OT");
        Statement st = conexion.createStatement();
        String query1 = "SELECT *FROM PRODUCTO";
        ResultSet rs = st.executeQuery(query1);
        //valido que el codigo del producto ingresado sea valido
        while (rs.next()) {
            id = rs.getInt(3);
            validarid = Valida(idProd, id);
            validacion.add(validarid);
        }
        rs.close();
        st.close();
        if (validacion.contains(true)) {
            Statement st2 = conexion.createStatement();
            String sql1 = "SELECT*FROM PRODUCTO WHERE codprod=";
            sql1 += idProd;
            //hago esta consulta para tener las existencias y el costo del producto seleccionado por el cliente.
            ResultSet rs2 = st2.executeQuery(sql1);
            while (rs2.next()) {
                existencias = rs2.getInt(5);
                costo = rs2.getFloat(6);
            }
            rs2.close();
            st2.close();
            stock = existencias - cantidad;
            //si la cantidad ingresada por el cliente es mayor de la que dispone la empresa no se puede realizar la
            //compra
            if (stock <= 0) {
                return "NO HAY EXISTENCIAS DE ESTE PRODUCTO";
            }
            //luego de verificar que hay existencias, actualizo el valor en la tabla producto
            String sql2 = "SELECT existencias FROM PRODUCTO where codprod=";
            sql2 += idProd;
            PreparedStatement psaux = conexion.prepareStatement(sql2);
            psaux.setInt(1, stock);
            psaux.executeUpdate();
            psaux.close();
            //inserto los datos ingresados por los usuarios en la tabla vendido para generar posteriormente el reporte
            //de ventas
            String sql3 = "INSERT INTO VENDIDO VALUES (?,?,?,?)";
            PreparedStatement pst = conexion.prepareStatement(sql3);
            pst.setInt(1, idvent);
            pst.setInt(2, idProd);
            pst.setInt(3, cantidad);
            pst.setFloat(4, costo);
            pst.executeUpdate();
            pst.close();
            //UNA VEZ QUE YA INGRESO LOS DATOS DEL PRODUCTO A COMPRAR VUELVE A PEDIR QUE INGRESE MAS DATOS EN CASO
            //QUE EL CLIENTE QUIERA HACER MAS DE UNA OPERACION, PARA DETENER ESTA OPERACION SE DEBE INGRESAR LA OPCION
            //TERMINAR VENTA QUE ESTA EN VENTA.HTML
            return "redirect:venta.html";
            //

        }
       return "El codigo del producto ingresado no existe";
    }
    //Esta funcion valida que el codigo ingresado por el administrador exista en la base de datos
    //en caso contrario no ejecuta las consultas a la base de datos.
    public Boolean Valida(int codigoIngresa, int codigoBD) {
        return codigoBD == codigoIngresa;
    }


}




