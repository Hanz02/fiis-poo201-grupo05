package uni.fiis.poo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Scanner;
//Clase Electricidad
public class Electricidad implements Producto {
    //Numero de productos de electricidad en stock disponibles
    private int NumBombillas=3,NumLinternas=7,NumTomacorr=9,NumTubElec=0;
    //Costo en soles
    private float CostoBombillas=6.5f,CostoLinternas=8.94f,CostoTomacorr=1.2f,CostoTubElec=3.57f;


    Collection<Integer> StockProdElectric = new ArrayList<>();
    Collection<String> ProductosElectricos=new ArrayList<>();
    Collection<Float> CostoProdElectric =new ArrayList<>();

    public boolean Validastock() {
        boolean stock=true;
        if(NumLinternas==0){
            System.out.println("No hay linternas disponibles");
            stock=false;
        }
        if(NumTubElec==0){
            System.out.println("No hay tubos electricos disponibles");
            stock=false;
        }
        if(NumTomacorr==0){
            System.out.println("No hay toma corrientes disponibles");
            stock=false;
        }
        if(NumBombillas==0){
            System.out.println("No hay bombillas disponibles");
            stock=false;
        }
        return stock;

    }
    public float Restastock(){
        int opc;
        boolean stock2;
        float costo=0.0f;
        stock2=Validastock();
        Scanner sc=new Scanner(System.in);
        StockProdElectric.add(NumBombillas);
        StockProdElectric.add(NumLinternas);
        StockProdElectric.add(NumTomacorr);
        StockProdElectric.add(NumTubElec);
        ProductosElectricos.add("Bombillas");
        ProductosElectricos.add("Linternas");
        ProductosElectricos.add("Tomacorrientes:");
        ProductosElectricos.add("Tubos electricos");
        if(stock2) {
            System.out.println("¿Que producto del area electricidad desea?");
            System.out.println("1.Bombillas, costo:6.5 soles");
            System.out.println("2.Linternas, costo:8.94 soles");
            System.out.println("3.Tomacorrientes, costo:1.2 soles");
            System.out.println("4.Tubos electricos, costo:3.57 soles");
            opc = sc.nextInt();
            switch (opc) {
                case 1:
                    System.out.println("Usted escogio Bombillas");
                    CostoProdElectric.add(CostoBombillas);
                    costo = CostoBombillas;
                    NumBombillas--;
                    break;
                case 2:
                    System.out.println("Usted escogio Linternas");
                    CostoProdElectric.add(CostoLinternas);
                    costo = CostoLinternas;
                    NumLinternas--;
                    break;
                case 3:
                    System.out.println("Usted escogio Tomacorrientes");
                    NumTomacorr--;
                    CostoProdElectric.add(CostoTomacorr);
                    costo = CostoTomacorr;
                    break;
                case 4:
                    System.out.println("Usted escogio Tubos electricos");
                    CostoProdElectric.add(CostoTubElec);
                    costo = CostoTubElec;
                    NumTubElec--;
                    break;
                default:
                    if (CostoProdElectric.isEmpty()) {
                        System.out.println("Usted no escogio nada");
                        costo = 0.0f;
                    }
                    break;
            }
        }
        else{
            System.out.println("ALERTA, NO HAY STOCK EN UNO O MAS PRODUCTOS");
            MostrarDatos();
        }
        return costo;
    }


    public void MostrarDatos() {
        Iterator i = StockProdElectric.iterator();
        Iterator r = ProductosElectricos.iterator();
        while((i.hasNext())||(r.hasNext())){
            System.out.println(r.next());
            System.out.println(i.next());
        }
    }
    public void CostoTotal() {
        Iterator i= CostoProdElectric.iterator();
        while(i.hasNext()){
            System.out.println(i.next());

        }
    }
}