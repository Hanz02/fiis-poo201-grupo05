+CREATE DATABASE FERRETERIA;

+USE FERRETERIA;

+CREATE TABLE ADMIN(

+codigo int primary key NOT NULL,

+clave int NOT NULL

+);

+CREATE TABLE ELECTRICIDAD(

+NumBombillas int,

+NumLinternas int,

+NumTomacorr int,

+NumTubElec int,

+CostoBombillas float,

+Costo Linternas float,

+CostoTomacorr float,

+CostoTubElec float

+);

+CREATE TABLE SEGURIDAD(

+NumLlaves int,

+NumCerraduras int,

+NumZapatos int,

+NumCandados int,

+CostoLlaves float,

+CostoCerraduras float,

+CostoZapatos float,

+CostoCandados float

+);

+CREATE TABLE COMPRADOR(

+dni varchar(9),

+nombre varchar(20),

+apellido varchar(10)

+);

+CREATE TABLE GASFITERIA(

+NumFiltSarro int,

+NumFiltTuberias int,

+NumMangueras int,

+NumPurifAgua int,

+CostoFiltSarro float,

+CostoTuberias float,

+CostoMangueras float,

+CostoPurifAgua float

+);

+CREATE TABLE PINTURA(

+Color varchar(10),

+NumPintura int,

+NumEspatula int,

+NumProtector int,

+NumLija int,

+NumBrocha int,

+NumRodillo int,

+NumBandeja int,

+PrecioPintura float,

+PrecioEspatula float,

+PrecioProtector float,

+PrecioLija float,

+PrecioBrocha float,

+PrecioRodillo float,

+PrecioBandeja float

+);

+INSERT INTO ADMIN VALUES (100,200);

+INSERT INTO PINTURA VALUES ('AMARILLO',12,5,3,10,12,7,6,20.00,3.90,6.50,1.90,9.90,12.90,4.90);

+INSERT INTO GASFITERIA VALUES (13,10,16,30,12.4,20.00,10.50,32.40)

+INSERT INTO ELECTRICIDAD VALUES (3,7,9,0,6.5,8.94,1.2,3.57);

+INSERT INTO SEGURIDAD VALUES (10,7,0,4,7,14.5,8,99);

