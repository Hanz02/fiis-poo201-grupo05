package edu.uni.poo.lab.llllllllllll;

public class Aeronave {
    String fabricante;
    String modelo;
    String capacidad;
    String fecha_compra;

    public Aeronave(String fabricante, String modelo, String capacidad, String fecha_compra){
        this.fabricante = fabricante;
        this.modelo = modelo;
        this.capacidad = capacidad;
        this.fecha_compra = fecha_compra;
    }
    public void mostrar_datos(){
        System.out.println("- Fabricante: " + this.fabricante);
        System.out.println("- Modelo: " + this.modelo);
        System.out.println("- Capacidad: " + this.capacidad);
        System.out.println("- Fecha de compra: " + this.fecha_compra);
    }
}

