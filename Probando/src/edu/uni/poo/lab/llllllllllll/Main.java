package edu.uni.poo.lab.llllllllllll;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int respuesta;

        Scanner mn = new Scanner(System.in);

        Aeronave aeronave1 = new Aeronave("Antonio Diestra","Boeing 737","150 personas","15/12/2018");
        Aeronave aeronave2 = new Aeronave("Miguel García", "Airbus A320","140 personas","17/08/2018");
        Aeronave aeronave3 = new Aeronave("Hanz Ortega", "Boeing 777","160 personas","29/02/2016");
        Aeronave aeronave4 = new Aeronave("Edwin MP","Diamond DA42","180 personas","12/01/2019");

        System.out.println("Elija la aeronave: ");
        System.out.println("1.");
        System.out.println("2.");
        System.out.println("3.");
        System.out.println("4.");

        do{
            respuesta = mn.nextInt();
        } while(respuesta>4 || respuesta<1);
        switch (respuesta){
            case 1 -> aeronave1.mostrar_datos();
            case 2 -> aeronave2.mostrar_datos();
            case 3 -> aeronave3.mostrar_datos();
            case 4 -> aeronave4.mostrar_datos();
        }
    }
}
