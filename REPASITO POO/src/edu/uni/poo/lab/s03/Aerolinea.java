package edu.uni.poo.lab.s03;

public class Aerolinea {

    String pag_web;
    String direccion;
    String corr_elect;
    int telef_celular;
    int telef_fijo;
    String nomb_aerolinea;

    public Aerolinea(String pag_web, String direccion, String corr_elect, int telef_celular, int telef_fijo, String nomb_aerolinea) {
        this.pag_web = pag_web;
        this.direccion = direccion;
        this.corr_elect = corr_elect;
        this.telef_celular = telef_celular;
        this.telef_fijo = telef_fijo;
        this.nomb_aerolinea = nomb_aerolinea;
    }

    public void mostrarDatos(){
        System.out.println("-Nombre: " + this.nomb_aerolinea);
        System.out.println("-Telefono fijo: " + this.telef_fijo);
        System.out.println("-Telefono celular: " + this.telef_celular);
        System.out.println("-Correo Electronico: " + this.corr_elect);
        System.out.println("-Direccion: " + this.direccion);
        System.out.println("-Pagina web: " + this.pag_web);
    }

}
