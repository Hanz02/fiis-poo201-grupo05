package edu.uni.poo.lab.s03;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int resp;

        Scanner sc = new Scanner(System.in);

        //DATOS YA ESTABLECIDOS
        Aerolinea aerolinea1 = new Aerolinea("WWW.LAN.COM","CALLE MARIA ANTONIA 423","LAN@GMAIL.COM",947467366,6213123,"LAN");
        Aerolinea aerolinea2 = new Aerolinea("WWW.VUELAPERU.COM","CALLE LA BERNARDA 123","VUELAPERU@HOTMAIL.COM",964351621,4352374,"VUELA PERU");
        Aerolinea aerolinea3 = new Aerolinea("WWW.VIVAAIRPERU.COM","JIRON LA CASTILLA 890","VIVAPERU@HOTMAIL.COM",910234813,5408761,"VIVA AIR PERU");

        System.out.println("ELIJA LA AEROLINEA DE SU PREFERENCIA.....");
        System.out.println("1. LAN");
        System.out.println("2. VUELAPERU");
        System.out.println("3. VIVA AIR PERU");
        do {
            resp = sc.nextInt();
        }while(resp>3 || resp<1);


        switch (resp) {
            case 1 -> aerolinea1.mostrarDatos();
            case 2 -> aerolinea2.mostrarDatos();
            case 3 -> aerolinea3.mostrarDatos();
        }

    }

}
