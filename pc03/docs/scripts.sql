CREATE TABLE CAB(
id int PRIMARY KEY,
license_plate varchar(32),
id_car_model int,
manufacture_ye int,
owner_id int,
active bool
);
CREATE TABLE CAR_MODEL(
id int,
id_car_model int PRIMARY KEY,
model_name varchar(64),
model_description text,
CONSTRAINT fk_id FOREIGN KEY(id) REFERENCES CAB(id)
);
CREATE TABLE DRIVER(
ID_COND int PRIMARY KEY,
nombre varchar(128),
apellido varchar(128),
fechanac varchar(128),
licensenumb varchar(128),
fechaexp varchar(128),
working boolean
);
CREATE TABLE SHIFT(
id int PRIMARY KEY,
driver_id int,
cab_id int,
shift_start timestamp,
shift_end timestamp,
login_time timestamp,
logout_time timestamp,
FOREIGN KEY (driver_id) REFERENCES DRIVER (ID_COND),
FOREIGN KEY (cab_id) REFERENCES CAB (id)
);

INSERT INTO CAB VALUES (100,'XHQ123',110,1980,111,true);
INSERT INTO CAB VALUES (200,'TYS456',210,1997,211,false);
INSERT INTO CAR_MODEL VALUES (100,110,'TOYOTA','AUTO COMODO');
INSERT INTO CAR_MODEL VALUES (200,210,'NISSAN','SOLO EXISTE UN MODELO DISPONIBLE');
INSERT INTO DRIVER VALUES(123,'juan','perez','12-12-1990',12412,'12-01-2021',true);
INSERT INTO DRIVER VALUES(121,'jorge','sanchez','2-03-1995',52341,'22-12-2020',false);
INSERT INTO SHIFT VALUES(131,123,100,'2016-06-22 19:10:25-07','2016-06-22 19:10:25-07','2016-06-22 19:10:25-07','2016-06-22 19:10:25-07');
INSERT INTO SHIFT VALUES(111,121,200,'2016-06-22 19:10:25-07','2016-06-22 19:10:25-07','2016-06-22 19:10:25-07','2016-06-22 19:10:25-07');