package uni.fiis.poo;

public class Libro extends Estudiable {
    int codigo;
    String titulo;
    String autor;
    int num_paginas;
    int anio_publ;
    String descripcion;
    public Libro(int tiempo) {
        super(tiempo);
    }

    public Libro(int tiempo, int codigo, String titulo, String autor, int num_paginas, int anio_publ, String descripcion) {
        super(tiempo);
        this.codigo = codigo;
        this.titulo = titulo;
        this.autor = autor;
        this.num_paginas = num_paginas;
        this.anio_publ = anio_publ;
        this.descripcion = descripcion;
    }
    public void MostrarDatos(){
        //En el metodo Main debe multiplicarse el tiempo de estudiable por el numero de paginas
        System.out.println("El codigo es:"+codigo);
        System.out.println("El titulo es:"+titulo);
        System.out.println("El autor es:"+autor);
        System.out.println("El numero de paginas es:"+num_paginas);
        System.out.println("El anio de publicacion es:"+anio_publ);
        System.out.println("La descripcion es:"+descripcion);

    }
}
