package uni.fiis.poo;

public class Main {

    public static void main(String[] args) {
        //Obteniendo el numero de elementos de la pila:
        AdministradorPilas opcion=new AdministradorPilas();
        opcion.obtenerPila();
        if(opcion.num==1)
        {
            PilaImpArr ejemplo = new PilaImpArr();
            ejemplo.push();
            ejemplo.mostrarElementos();
            ejemplo.pop();
            ejemplo.EstaVacia();
        }
        if(opcion.num==2) {
            //Para la otra clase PilaImpVar:
            PilaImpVar ejemplo2 = new PilaImpVar();
            ejemplo2.push();
            ejemplo2.mostrarElementos();
            ejemplo2.pop();
            ejemplo2.EstaVacia();
        }
    }
    //La clase obtenerPila permite que Main este libre de codigo que normalmente se usaria para
    //recibir la opcion del usuario ademas que permite su reutilizacion.
}
