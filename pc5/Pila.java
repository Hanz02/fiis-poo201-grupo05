package uni.fiis.poo;

public interface Pila {
    void push();
    void pop();
    void EstaVacia();
    void mostrarElementos();
}
