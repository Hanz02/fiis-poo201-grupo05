package uni.fiis.poo;
import java.util.Scanner;

public class PilaImpArr implements Pila {
    protected int[] valor=new int[3];

    public void push() {

        Scanner sc=new Scanner(System.in);
        System.out.println("Ingrese 3 numeros enteros");
        for(int i=0;i<3;i++) {
            valor[i]=sc.nextInt();
        }
    }


    public void pop() {
        int b=2;
        System.out.println("Eliminando un elemento de la pila");
        if(b>=0){
            valor[b]=0;
            b--;
        }

    }


    public void EstaVacia() {
        if(valor[0]!=0){
            System.out.println("VERDADERO");
        }
        else{
            System.out.println("FALSO");
        }

    }


    public void mostrarElementos() {
        System.out.println("Los elementos de la pila son:");
        for(int i=0;i<3;i++){
            System.out.println(valor[i]);
        }
    }
}
