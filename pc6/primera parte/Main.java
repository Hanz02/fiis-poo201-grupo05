package uni.fiis.poo;

public class Main {

    public static void main(String[] args) {
        //Obteniendo el numero de elementos de la pila:
        AdministradorPilas opcion = new AdministradorPilas();
        opcion.obtenerPila();
        /* Probando la implementacion de la lista enlazada
        if(opcion.num==1)
        {
            PilaImpArr ejemplo = new PilaImpArr();
            ejemplo.push();
            ejemplo.mostrarElementos();
            ejemplo.pop();
            ejemplo.EstaVacia();
        }
        if(opcion.num==2) {
            //Para la otra clase PilaImpVar:
            PilaImpVar ejemplo2 = new PilaImpVar();
            ejemplo2.push();
            ejemplo2.mostrarElementos();
            ejemplo2.pop();
            ejemplo2.EstaVacia();
        }
    }*/
        if (opcion.num == 3) {
            PilaImpLst prueba =new PilaImpLst();
            prueba.AddFirst();
            prueba.removeFirst();
            prueba.iterator();
        }
        //Resulta mas sencillo usar las clases proporcionadas por java y ahorra
        //bastante tiempo.
    }
}
