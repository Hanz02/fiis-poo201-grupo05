package uni.fiis.poo;
/*No estoy implementando la clase PilaImpArr,PilaImpVar y PilaImpLst a la vez
ya que eso implicaria añadir metodos a la interface Pila o crear otra interface.
 */
public interface Pila {
    void AddFirst();
    void removeFirst();
    void iterator();

}
