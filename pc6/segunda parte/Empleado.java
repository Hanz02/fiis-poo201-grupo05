package uni.fiis.poo;
public class Empleado {
    protected int codigo,salario;
    protected String dni,nombre,apellido,FechContr,sup,dep;

    public Empleado(int codigo, int salario, String dni, String nombre, String apellido, String fechContr, String sup, String dep) {
        this.codigo = codigo;
        this.salario = salario;
        this.dni = dni;
        this.nombre = nombre;
        this.apellido = apellido;
        this.FechContr = fechContr;
        this.sup = sup;
        this.dep = dep;
    }

}
