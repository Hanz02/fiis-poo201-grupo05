package uni.fiis.poo;
//Esta clase de especializaciones de empleados hereda de empleado sus campos correspondientes
public class EspEmp extends Empleado {
    protected String NombCurso,FechI,FechF,universidad;

    public EspEmp(int codigo, int salario, String dni, String nombre, String apellido, String fechContr, String sup, String dep) {
        super(codigo, salario, dni, nombre, apellido, fechContr, sup, dep);
    }

    public EspEmp(int codigo, int salario, String dni, String nombre, String apellido, String fechContr, String sup, String dep, String nombCurso, String fechI, String fechF, String universidad) {
        super(codigo, salario, dni, nombre, apellido, fechContr, sup, dep);
        this.NombCurso = nombCurso;
        this.FechI = fechI;
        this.FechF = fechF;
        this.universidad = universidad;
    }
    public void MostrarDatos(){
            System.out.println("Codigo:"+codigo);
            System.out.println("Salario:"+salario);
            System.out.println("Dni:"+dni);
            System.out.println("Nombre:"+nombre);
            System.out.println("Apellido:"+apellido);
            System.out.println("Fecha de contratacion:"+FechContr);
            System.out.println("Supervisor a cargo:"+sup);
            System.out.println("Departamento:"+dep);
            System.out.println("Nombre del curso al cual pertenece el empleado:"+NombCurso);
            System.out.println("Fecha de inicio:"+FechI);
            System.out.println("Fecha de finalizacion del curso"+FechF);
            System.out.println("Universidad: "+universidad);

    }
}