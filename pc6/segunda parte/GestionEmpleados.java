package uni.fiis.poo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Scanner;
//Clase requerida que emplea colecciones para almacenar un conjunto de empleados
public class GestionEmpleados extends EspEmp {
    public GestionEmpleados(int codigo, int salario, String dni, String nombre, String apellido,
                            String fechContr, String sup, String dep, String nombCurso, String fechI, String fechF,
                            String universidad) {
        super(codigo, salario, dni, nombre, apellido, fechContr, sup, dep, nombCurso, fechI, fechF, universidad);
    }
    EspEmp pruebalocal =new EspEmp(codigo,salario,dni,nombre,apellido,FechContr,sup,dep,NombCurso,FechI,
            FechF,universidad);
    //Creo dos colecciones; Empleado y Especializaciones.
    Collection<Object> Empleado=new ArrayList<>();
    Collection<String> Especializaciones=new ArrayList<>();

    public void IngreDatos(){
        Scanner sc=new Scanner(System.in);
        System.out.println("Ingrese el codigo del empleado");
        pruebalocal.codigo=sc.nextInt();
        System.out.println("Ingrese el salario:");
        pruebalocal.salario=sc.nextInt();
        System.out.println("Ingrese el dni:");
        pruebalocal.dni=sc.nextLine();
        System.out.println("Ingrese el nombre");
        pruebalocal.nombre=sc.nextLine();
        System.out.println("Ingrese el apellido:");
        pruebalocal.apellido=sc.nextLine();
        System.out.println("Ingrese la fecha de contratacion:");
        pruebalocal.FechContr=sc.nextLine();
        System.out.println("Ingrese el nombre del superior a cargo:");
        pruebalocal.sup=sc.nextLine();
        System.out.println("Ingrese el departamento del empleado:");
        pruebalocal.dep=sc.nextLine();
        System.out.println("Ingrese el nombre del curso:");
        pruebalocal.NombCurso=sc.nextLine();
        System.out.println("Ingrese la fecha de inicio del curso:");
        pruebalocal.FechI=sc.nextLine();
        System.out.println("Ingrese la fecha de finalizacion del curso:");
        pruebalocal.FechF=sc.nextLine();
        System.out.println("Ingrese la universidad:");
        pruebalocal.universidad=sc.nextLine();
        //Almacenando a la coleccion el objeto actual.
        Empleado.add(pruebalocal);
    }
    public void modifDatos(int codigo2){
        int opc,salario2;
        Scanner sc3=new Scanner(System.in);
        if(Empleado.contains(codigo2)){
            System.out.println("Elija el campo a modificar:");
            System.out.println("1.CODIGO");
            System.out.println("2.SALARIO");
            opc=sc3.nextInt();
            if(opc==1){
                System.out.println("Ingrese el nuevo codigo:");
                codigo2=sc3.nextInt();

                System.out.println("MODIFICACION DE CODIGO REALIZADA CON EXITO.");
            }
            if(opc==2){
                System.out.println("Ingrese el nuevo salario");
                salario2=sc3.nextInt();
                pruebalocal.salario=salario2;
                System.out.println("MODIFICACION DE SALARIO REALIZADA CON EXITO.");
            }
        }
        else{
            System.out.println("El empleado no existe");
        }
    }
    public void EliminarEmpleado(int codigo2){
        int opc;
        Scanner sc4=new Scanner(System.in);
        if(Empleado.contains(codigo2)){
            System.out.println("¿Esta seguro que desea eliminar al empleado?");
            System.out.println("1.SI");
            System.out.println("2.NO");
            opc=sc4.nextInt();
            if(opc==1){
                Empleado.remove(pruebalocal);
            }
            if(opc==2){
                System.out.println("PROGRAMA TERMINADO CON EXITO");
            }
        }
        else{
            System.out.println("El empleado no existe");
        }

    }
    public void GestEspecial(){
        Scanner sc4=new Scanner(System.in);
        int numespec,opc;
        String espec=null;
        System.out.println("Desea aniadir o eliminar especializaciones?");
        System.out.println("1.Aniadir");
        System.out.println("2.Eliminar");
        opc=sc4.nextInt();
        if(opc==1) {
            System.out.println("¿Cuantas especializaciones desea ingresar?");
            numespec = sc4.nextInt();
            for (int i = 0; i < numespec; i++) {
                System.out.println("Ingrese las nuevas especializaciones");
                espec=sc4.nextLine();
                Especializaciones.add(espec);
            }
        }
        if(opc==2){
            System.out.println("Ingrese la especializacion a eliminar:");
            espec=sc4.nextLine();
            Especializaciones.remove(espec);
        }

    }
    public void mostrarDatos(){
        int opc,opc2,codigo;
        Scanner sc=new Scanner(System.in);
        System.out.println("Elija una opcion");
        System.out.println("1.Mostrar datos de un empleado");
        System.out.println("2.Mostrar datos de todos los empleados");
        opc=sc.nextInt();
        if(opc==1){
            System.out.println("Ingrese el codigo del empleado");
            codigo=sc.nextInt();
            if(Empleado.contains(codigo)) {
                pruebalocal.MostrarDatos();
            }
            else{
                System.out.println("El empleado no existe");
            }
            System.out.println("Desea ver los datos de todos los empleados?");
            System.out.println("1.SI");
            System.out.println("2.NO");
            opc2=sc.nextInt();
            if(opc2==1){
                System.out.println("Los datos de los empleados son:");
                Iterator i=Empleado.iterator();
                while(i.hasNext()){
                    System.out.println(i.next());
                }

            }
            else{
                System.out.println("¡¡¡¡¡¡¡PROGRAMA TERMINADO CON EXITO!!!!!!!");
            }
        }
        if(opc==2){
            System.out.println("Los datos de los empleados son:");
            Iterator i=Empleado.iterator();
            while(i.hasNext()){
                System.out.println(i.next());
            }
        }
    }
    public void LiberaMemoria(){
        Especializaciones.clear();
        Empleado.clear();
        System.out.println("PC6 CONCLUIDA CON EXITO");
    }

}
