
package uni.fiis.poo;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {

        //Colecciones
        //Agregar Empleado:
        AgEmpleado();
        //Modificar dato del empleado:
        ModDatEmpleado();
        //Eliminar empleado:
        ElimEmpleado();
        //Gestionar las Especializaciones:
        GestEspecial();
        //Mostrar Informacion:
        MostInfo();

    }
    public static void AgEmpleado(){
        //Leer datos del teclado
        Scanner sc=new Scanner(System.in);
        int codigo,salario,n;
        String dni,nombre,apellido,FechContr,sup,dep,NombCurso,FechI,FechF,universidad;
        GestionEmpleados prueba=new GestionEmpleados(codigo=0,salario=0,dni=null,nombre=null,apellido=null,FechContr=null,sup=null
                , null,NombCurso=null,FechI=null,
                FechF=null,universidad=null);
        System.out.println("Cuantos empleados desea ingresar?");
        n=sc.nextInt();
        for(int i=0;i<n;i++){
            prueba.IngreDatos();
        }

    }
    public static void ModDatEmpleado(){
        //Modificar un dato determinado usar switch case o puro if
        Scanner sc2=new Scanner(System.in);
        int codigo,salario,codigo2;
        String dni,nombre,apellido,FechContr,sup,dep,NombCurso,FechI,FechF,universidad;
        GestionEmpleados prueba2=new GestionEmpleados(codigo=0,salario=0,dni=null,nombre=null,apellido=null,FechContr=null,sup=null
                , null,NombCurso=null,FechI=null,
                FechF=null,universidad=null);
        System.out.println("Ingrese el codigo del empleado a modificar");
        codigo2=sc2.nextInt();
        prueba2.modifDatos(codigo2);



    }
    public static void ElimEmpleado(){
        //Busca por codigo y elimina un empleado, mostrar un mensaje de confirmacion antes de borrar
        Scanner sc2=new Scanner(System.in);
        int codigo,salario,codigo2;
        String dni,nombre,apellido,FechContr,sup,dep,NombCurso,FechI,FechF,universidad;
        GestionEmpleados prueba2=new GestionEmpleados(codigo=0,salario=0,dni=null,nombre=null,apellido=null,FechContr=null,sup=null
                , null,NombCurso=null,FechI=null,
                FechF=null,universidad=null);
        System.out.println("Ingrese el codigo del empleado a eliminar");
        codigo2=sc2.nextInt();
        prueba2.EliminarEmpleado(codigo2);
    }
    public static void GestEspecial(){
        //Debe agregar o eliminar las especializacionesScanner sc=new Scanner(System.in);
                int codigo,salario,n;
                String dni,nombre,apellido,FechContr,sup,dep,NombCurso,FechI,FechF,universidad;
                GestionEmpleados prueba=new GestionEmpleados(codigo=0,salario=0,dni=null,nombre=null,apellido=null,FechContr=null,sup=null
                        , null,NombCurso=null,FechI=null,
                        FechF=null,universidad=null);
                prueba.GestEspecial();


    }
    public static void MostInfo(){
        int codigo,salario,opc,opc2;
        String dni,nombre,apellido,FechContr,sup,dep,NombCurso,FechI,FechF,universidad;
        GestionEmpleados prueba=new GestionEmpleados(codigo=0,salario=0,dni=null,nombre=null,apellido=null,FechContr=null,sup=null
                , null,NombCurso=null,FechI=null,
                FechF=null,universidad=null);
        //Muestra un empleado o todos los empleados
        prueba.mostrarDatos();
        prueba.LiberaMemoria();

    }
}
