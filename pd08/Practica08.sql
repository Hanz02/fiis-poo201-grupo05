USE DATABASE;
CREATE TABLE EMPLOYEES(
employee_id integer not null,
first_name varchar(20),
last_name varchar(20),
email varchar(10),
phone_number varchar(9),
hire_date date,
job_id integer not null,
salary float,
commission_pct float,
manager_id integer not null,
department_id integer not null,
PRIMARY KEY(employee_id,job_id,manager_id,department_id);
);
CREATE TABLE JOBS(
job_id integer not null,
job_tittle varchar(10),
min_salary float,
max_salary float,
CONSTRAINT fk_Employees_job_id FOREIGN KEY (job_id) REFERENCES EMPLOYEES(job_id)
);
CREATE TABLE JOB_HISTORY(
employee_id integer not null,
start_date date,
end_date date,
job_id integer not null,
department_id integer not null,
CONSTRAINT fk_EMPLOYEES_employee_id FOREIGN KEY (employee_id) REFERENCES EMPLOYEES(employee_id),
CONSTRAINT fk_EMPLOYEES_job_id FOREIGN KEY (job_id) REFERENCES EMPLOYEES(job_id),
CONSTRAINT fk_EMPLOYEES_department_id FOREIGN KEY (department_id) REFERENCES EMPLOYEES(department_id)
);
CREATE TABLE DEPARTMENTS(
department_id integer not null,
department_name varchar(15),
manager_id integer not null,
location_id integer not null PRIMARY KEY,
CONSTRAINT fk_EMPLOYEES_department_id FOREIGN KEY (department_id) REFERENCES EMPLOYEES(department_id),
CONSTRAINT fk_EMPLOYEES_manager_id FOREIGN KEY (manager_id) REFERENCES EMPLOYEES(manager_id)
);
CREATE TABLE LOCATIONS(
location_id integer not null,
street_adress varchar(15),
postal_code integer,
city varchar(10),
state_province varchar(10),
country_id integer not null PRIMARY KEY,
CONSTRAINT fk_DEPARTMENTS_location_id FOREIGN KEY (location_id) REFERENCES DEPARTMENTS(location_id)
);
CREATE TABLE COUNTRIES(
country_id integer not null,
country name varchar(10),
region_id integer not null PRIMARY KEY,
CONSTRAINT fk_LOCATIONS_country_id FOREIGN KEY (country_id) REFERENCES LOCATIONS(country_id)
);
CREATE TABLE REGIONS(
region_id integer not null,
region_name varchar(10),
CONSTRAINT fk_COUNTRIES_region_id FOREIGN KEY (region_id) REFERENCES COUNTRIES(region_id)
);
CREATE TABLE ORDERS(
order_id integer not null PRIMARY KEY,
order_date datetime,
order_mode varchar(10),
customer_id integer not null,
order_status boolean,
order_total integer,
sales_rep_id integer not null,
promotion_id integer not null
);
CREATE TABLE ORDER_ITEMS(
order_id integer not null,
line_item_id integer,
product_id integer not null PRIMARY KEY,
unit_price float,
quantity integer,
CONSTRAINT fk_ORDERS_order_id FOREIGN KEY (order_id) REFERENCES ORDERS(order_id)
);
CREATE TABLE PRODUCT_INFORMATION(
product_id integer not null,
product_name varchar(10),
product_description varchar(20),
category_id integer,
weight_class varchar(5),
warranty_period float,
supplier_id integer,
product_status varchar(5),
list_price float,
min_price float,
catalog_url varchar(20)
CONSTRAINT fk_ORDERS_ITEMS_product_id FOREIGN KEY (product_id) REFERENCES ORDERS_ITEMS(order_id)
);
CREATE TABLE PRODUCT_DESCRIPTIONS(
product_id integer not null,
language_id integer,
translated_name varchar(20),
translated_description varchar(30),
CONSTRAINT fk_ORDERS_ITEMS_product_id FOREIGN KEY (product_id) REFERENCES ORDERS_ITEMS(product_id)
);
CREATE TABLE INVENTORIES(
product_id integer not null,
warehouse_id integer not null PRIMARY KEY,
quantity_on_hand integer,
CONSTRAINT fk_ORDERS_ITEMS_product_id FOREIGN KEY (product_id) REFERENCES ORDERS_ITEMS(product_id)
);
CREATE TABLE WAREHOUSES(
warehouse_id integer not null,
warehouse_spec varchar(20),
warehouse_name varchar(20),
location_id integer not null,
wh_geo_location varchar(30)
CONSTRAINT fk_INVENTORIES_warehouse_id FOREIGN KEY (warehouse_id) REFERENCES INVENTORIES(warehouse_id),
CONSTRAINT fk_LOCATIONS_location_id FOREIGN KEY (location_id) REFERENCES LOCATIONS(location_id)
);
CREATE TABLE CUSTOMERS(
employee_id integer not null,
order_id integer not null,
customer_id integer not null PRIMARY KEY,
cust_first_name varchar(20),
cust_last_name varchar(20),
cust_adress varchar(20),
phone_number varchar(9),
nls_language varchar(10),
nls_territory varchar(15),
credit_limit float,
cust_email varchar(20),
account_mgr_id integer,
cust_geo_location varchar(30),
date_of_birth date,
marital_status varchar(10),
gender varchar(10),
income_level varchar(10)
CONSTRAINT fk_EMPLOYEES_employee_id FOREIGN KEY (employee_id) REFERENCES EMPLOYEES(employee_id),
CONSTRAINT fk_ORDERS_order_id FOREIGN KEY (order_id) REFERENCES ORDERS(order_id)
);
