import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Connection;
import java.sql.PreparedStatement;

@Controller
public class pd10 {
    @Autowired
    JdbcTemplate template;
    @RequestMapping("/registroest")
    public String registrarestudiante(@RequestParam String nombre, @RequestParam String apellidop,
                                      @RequestParam String apellidom, @RequestParam String universidad,
                                      @RequestParam String especialidad,
                                      @RequestParam String codigo)throws Exception{
        Connection con = template.getDataSource().getConnection();
        String sql = "INSERT INTO ESTUDIANTE(NOMBRE, APELLIDOP, APELLIDOM, UNIVERSIDAD, ESPECIALIDAD, CODIGO) VALUES(?, ?, ?, ?, ?, ?)";
        PreparedStatement pst = con.prepareStatement(sql);
        pst.setString(1, nombre);
        pst.setString(2, apellidop);
        pst.setString(3, apellidom);
        pst.setString(4, universidad);
        pst.setString(5, especialidad);
        pst.setString(6, codigo);
        pst.executeUpdate();
        return "redirect:/validacion.html";
    }
}