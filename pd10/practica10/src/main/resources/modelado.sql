CREATE TABLE ESTUDIANTE(
codEst  int PRIMARY KEY,
Nombre varchar(30),
Apellido varchar(30),
email varchar(20),
contrasenia varchar(20)
);
CREATE TABLE CursoEscogido(
codEst int IDENTITY(1,1),
CodCurso int,
NombCurso varchar(20),
CONSTRAINT fk_codEst FOREIGN KEY (codEst) REFERENCES ESTUDIANTE(codEst)
);
CREATE TABLE CURSO(
codCurso int,
NombCurso varchar(20),
TipoCurso varchar(20),
Precio float,
CONSTRAINT fk_codEst FOREIGN KEY (codEst) REFERENCES ESTUDIANTE(codEst)
);