package uni.fiis.poo;
public class Aerolinea {
    String pag_web;
    String direccion;
    String corr_elect;
    String telef_celular;
    String telef_fijo;
    String nomb_aerolinea;
    public Aerolinea(String pag_web, String direccion, String corr_elect, String telef_celular, String telef_fijo, String nomb_aerolinea) {
        this.pag_web = pag_web;
        this.direccion = direccion;
        this.corr_elect = corr_elect;
        this.telef_celular = telef_celular;
        this.telef_fijo = telef_fijo;
        this.nomb_aerolinea = nomb_aerolinea;
    }

}
