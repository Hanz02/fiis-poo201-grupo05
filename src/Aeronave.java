package uni.fiis.poo;
public class Aeronave extends Vuelo {
    String fabricante;
    String modelo;
    int capacidad;
    int fech_compra;

    public Aeronave(String pag_web, String direccion, String corr_elect, String telef_celular, String telef_fijo, String nomb_aerolinea, int codVuelo, String fecha, String horaSalida, String durac_vuelo, int numPasaj, String fabricante, String modelo, int capacidad, int fech_compra) {
        super(pag_web, direccion, corr_elect, telef_celular, telef_fijo, nomb_aerolinea, codVuelo, fecha, horaSalida, durac_vuelo, numPasaj);
        this.fabricante = fabricante;
        this.modelo = modelo;
        this.capacidad = capacidad;
        this.fech_compra = fech_compra;
    }
    public void MostrarDatos() {
        System.out.println("-Nombre: " +this.nomb_aerolinea);
        System.out.println("-Telefono fijo: " + this.telef_fijo);
        System.out.println("-Telefono celular: " + this.telef_celular);
        System.out.println("-Correo Electronico: " + this.corr_elect);
        System.out.println("-Direccion: " + this.direccion);
        System.out.println("-Pagina web: " + this.pag_web);
        System.out.println("-Codigo del vuelo: " + this.CodVuelo);
        System.out.println("-Fecha: " + this.Fecha);
        System.out.println("-Hora de salida: " + this.HoraSalida);
        System.out.println("-Duracion del vuelo: " + this.Durac_vuelo);
        System.out.println("-Numero de pasajeros: " + this.NumPasaj);
        System.out.println("-Fabricante:" + this.fabricante);
        System.out.println("-Modelo:" + this.modelo);
        System.out.println("-Capacidad:" + this.capacidad);
    }

}
