package uni.fiis.poo;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        System.out.println("SISTEMA DE RESERVA DE VUELOS");
        String alergias;
        System.out.println("Ingrese la alergia del cliente:");
        Scanner sc = new Scanner(System.in);
        alergias = sc.nextLine();
        if (alergias != null) {
            System.out.println("¡Alerta!, el cliente es alergico a:" + alergias);
        }
        Cliente cliente = new Cliente("74699748", "ELON TUSK", "pc1@uni.pe", 963784129);
        System.out.println("Los datos del cliente son:" + cliente);
        Aerolinea();
        System.out.println("---------------");
        System.out.println("Reserva creada con exito");
    }

    public static void Aerolinea() {
        int resp, fech_actual = 2020,aux;
        Scanner sc = new Scanner(System.in);
        Aeronave aeronave1 = new Aeronave("WWW.LAN.COM", "CALLE MARIA ANTONIA 423", "LAN@GMAIL.COM",
                "947467366", "6213123", "LAN", 100, "19/07/2019", "07:19",
                "1:30", 40, "ROSCOSMOS", "SI403", 50, 1970);
        Aeronave aeronave2 = new Aeronave("WWW.VUELAPERU.COM", "CALLE LA BERNARDA 123", "VUELAPERU@HOTMAIL.COM",
                "964351621", "4352374", "VUELA PERU", 001, "20/07/2019", "06:20"
                , "0:45", 60, "NASA", "SI302", 80, 1971);
        Aeronave aeronave3 = new Aeronave("WWW.VIVAAIRPERU.COM", "JIRON LA CASTILLA 890", "VIVAPERU@HOTMAIL.COM",
                "910234813", "5408761", "VIVA AIR PERU", 010, "30/07/2019", "10:34",
                "3:00", 80, "SPACEX", "SI505", 100, 1972);
        System.out.println("ELIJA LA AEROLINEA DE SU PREFERENCIA.....");
        System.out.println("1. LAN");
        System.out.println("2. VUELAPERU");
        System.out.println("3. VIVA AIR PERU");
        do {
            resp = sc.nextInt();
        } while (resp > 3 || resp < 1);
        switch (resp) {
            case 1:
                Aeronave aeronave4 = new Aeronave("WWW.LAN.COM", "CALLE MARIA ANTONIA 423", "LAN@GMAIL.COM",
                        "947467366", "6213123", "LAN", 100, "19/07/2019", "07:19",
                        "1:30", 40, "ROSCOSMOS", "SI403", 50, 1970);
                Aeronave aeronave5 = new Aeronave("WWW.LAN.COM", "CALLE MARIA ANTONIA 423", "LAN@GMAIL.COM",
                        "947467366", "6213123", "LAN", 200, "20/07/2019", "08:19",
                        "1:48", 60, "ESA", "IO2", 100, 1980);
                Aeronave aeronave6 = new Aeronave("WWW.LAN.COM", "CALLE MARIA ANTONIA 423", "LAN@GMAIL.COM",
                        "947467366", "6213123", "LAN", 300, "21/07/2019", "09:19",
                        "1:54", 50, "CONIDA", "IO1", 55, 1990);
                System.out.println("Escoja uno o mas vuelos.....");
                System.out.println("1. " + aeronave4.CodVuelo + aeronave4.Fecha + aeronave4.HoraSalida + aeronave4.Durac_vuelo + aeronave4.NumPasaj);
                System.out.println("2. " + aeronave5.CodVuelo + aeronave5.Fecha + aeronave5.HoraSalida + aeronave5.Durac_vuelo + aeronave5.NumPasaj);
                System.out.println("3. " + aeronave6.CodVuelo + aeronave6.Fecha + aeronave6.HoraSalida + aeronave6.Durac_vuelo + aeronave6.NumPasaj);
                aux = sc.nextInt();
                System.out.println("Los vuelos escogidos son:");
                switch (aux) {
                    case 1:

                        System.out.println(aeronave4.pag_web + aeronave4.CodVuelo+aeronave4.NumPasaj+aeronave4.modelo+aeronave4.fabricante);
                        aeronave4.fech_compra = fech_actual - aeronave4.fech_compra;
                        System.out.println("La antiguedad de la aeronave es:" + aeronave4.fech_compra);
                    case 2:
                        System.out.println(aeronave5.pag_web + aeronave5.CodVuelo+aeronave5.NumPasaj+aeronave5.modelo+aeronave5.fabricante);
                        aeronave5.fech_compra = fech_actual - aeronave5.fech_compra;
                        System.out.println("La antiguedad de la aeronave es:" + aeronave5.fech_compra);
                    case 3:
                        System.out.println(aeronave6.pag_web + aeronave6.CodVuelo+aeronave6.NumPasaj+aeronave6.modelo+aeronave6.fabricante);
                        aeronave6.fech_compra = fech_actual - aeronave6.fech_compra;
                        System.out.println("La antiguedad de la aeronave es:" + aeronave6.fech_compra);
                }
            case 2:
                Aeronave aeronave7 = new Aeronave("WWW.VUELAPERU.COM", "CALLE LA BERNARDA 123", "VUELAPERU@HOTMAIL.COM",
                        "964351621", "4352374", "VUELA PERU", 001, "20/07/2019", "06:20"
                        , "0:45", 60, "NASA", "SI302", 80, 1971);
                Aeronave aeronave8 = new Aeronave("WWW.VUELAPERU.COM", "CALLE LA BERNARDA 123", "VUELAPERU@HOTMAIL.COM",
                        "964351621", "4352374", "VUELA PERU", 002, "21/07/2019", "05:20"
                        , "0:15", 70, "ELIXIR", "FB402", 80, 1981);
                Aeronave aeronave9 = new Aeronave("WWW.VUELAPERU.COM", "CALLE LA BERNARDA 123", "VUELAPERU@HOTMAIL.COM",
                        "964351621", "4352374", "VUELA PERU", 003, "22/07/2019", "04:20"
                        , "0:30", 75, "NASA", "FB302", 80, 1991);
                System.out.println("Escoja uno o mas vuelos.....");
                System.out.println("1. " + aeronave7.CodVuelo + aeronave7.Fecha + aeronave7.HoraSalida + aeronave7.Durac_vuelo + aeronave7.NumPasaj);
                System.out.println("2. " + aeronave8.CodVuelo + aeronave8.Fecha + aeronave8.HoraSalida + aeronave8.Durac_vuelo + aeronave8.NumPasaj);
                System.out.println("3. " + aeronave9.CodVuelo + aeronave9.Fecha + aeronave9.HoraSalida + aeronave9.Durac_vuelo + aeronave9.NumPasaj);
                aux = sc.nextInt();
                switch (aux) {
                    case 1:
                        System.out.println(aeronave7.pag_web + aeronave7.CodVuelo+aeronave7.NumPasaj+aeronave7.modelo+aeronave7.fabricante);
                        aeronave7.fech_compra = fech_actual - aeronave7.fech_compra;
                        System.out.println("La antiguedad de la aeronave es:" + aeronave7.fech_compra);
                    case 2:
                        System.out.println(aeronave8.pag_web + aeronave8.CodVuelo+aeronave8.NumPasaj+aeronave8.modelo+aeronave8.fabricante);
                        aeronave8.fech_compra = fech_actual - aeronave8.fech_compra;
                        System.out.println("La antiguedad de la aeronave es:" + aeronave8.fech_compra);
                    case 3:
                        System.out.println(aeronave9.pag_web + aeronave9.CodVuelo+aeronave9.NumPasaj+aeronave9.modelo+aeronave9.fabricante);
                        aeronave9.fech_compra = fech_actual - aeronave9.fech_compra;
                        System.out.println("La antiguedad de la aeronave es:" + aeronave9.fech_compra);
                }
            case 3:
                Aeronave aeronave10 = new Aeronave("WWW.VIVAAIRPERU.COM", "JIRON LA CASTILLA 890", "VIVAPERU@HOTMAIL.COM",
                        "910234813", "5408761", "VIVA AIR PERU", 010, "30/07/2019", "10:34",
                        "3:00", 80, "SPACEX", "SI505", 100, 1972);
                Aeronave aeronave11 = new Aeronave("WWW.VIVAAIRPERU.COM", "JIRON LA CASTILLA 890", "VIVAPERU@HOTMAIL.COM",
                        "910234813", "5408761", "VIVA AIR PERU", 010, "31/07/2019", "08:34",
                        "3:30", 88, "ROSCOSMOS", "GE501", 100, 1982);
                Aeronave aeronave12 = new Aeronave("WWW.VIVAAIRPERU.COM", "JIRON LA CASTILLA 890", "VIVAPERU@HOTMAIL.COM",
                        "910234813", "5408761", "VIVA AIR PERU", 010, "29/07/2019", "06:34",
                        "3:40", 92, "NASA", "SI507", 100, 1992);
                System.out.println("Escoja uno o mas vuelos.....");
                System.out.println("1. " + aeronave10.CodVuelo + aeronave10.Fecha + aeronave10.HoraSalida + aeronave10.Durac_vuelo + aeronave10.NumPasaj);
                System.out.println("2. " + aeronave11.CodVuelo + aeronave11.Fecha + aeronave11.HoraSalida + aeronave11.Durac_vuelo + aeronave11.NumPasaj);
                System.out.println("3. " + aeronave12.CodVuelo + aeronave12.Fecha + aeronave12.HoraSalida + aeronave12.Durac_vuelo + aeronave12.NumPasaj);
                aux = sc.nextInt();
                switch (aux) {
                    case 1:
                        System.out.println(aeronave10.pag_web + aeronave10.CodVuelo+aeronave10.NumPasaj+aeronave10.modelo+aeronave10.fabricante);
                        aeronave10.fech_compra = fech_actual - aeronave10.fech_compra;
                        System.out.println("La antiguedad de la aeronave es:" + aeronave10.fech_compra);
                    case 2:
                        System.out.println(aeronave11.pag_web + aeronave11.CodVuelo+aeronave11.NumPasaj+aeronave11.modelo+aeronave11.fabricante);
                        aeronave11.fech_compra = fech_actual - aeronave11.fech_compra;
                        System.out.println("La antiguedad de la aeronave es:" + aeronave11.fech_compra);
                    case 3:
                        System.out.println(aeronave12.pag_web + aeronave12.CodVuelo+aeronave12.NumPasaj+aeronave12.modelo+aeronave12.fabricante);
                        aeronave12.fech_compra = fech_actual - aeronave12.fech_compra;
                        System.out.println("La antiguedad de la aeronave es:" + aeronave12.fech_compra);
                }
        }

    }
}


