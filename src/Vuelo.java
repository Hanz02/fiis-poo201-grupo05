package uni.fiis.poo;
public class Vuelo extends Aerolinea {
    protected int CodVuelo;
    protected String Fecha;
    protected String HoraSalida;
    protected String Durac_vuelo;
    protected int NumPasaj;
    //metodo para establecer las variables del objeto

    public Vuelo(String pag_web, String direccion, String corr_elect, String telef_celular, String telef_fijo, String nomb_aerolinea, int codVuelo, String fecha, String horaSalida, String durac_vuelo, int numPasaj) {
        super(pag_web, direccion, corr_elect, telef_celular, telef_fijo, nomb_aerolinea);
        CodVuelo = codVuelo;
        Fecha = fecha;
        HoraSalida = horaSalida;
        Durac_vuelo = durac_vuelo;
        NumPasaj = numPasaj;
    }
}




